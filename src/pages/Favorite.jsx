import ProductList from "../components/ProductList/ProductList"

export default function Favorite(props) {

  
  return  <ProductList page="favorite"  removeFromFavorite={props.removeFromFavorite}   item={props.productsFavorite} />

}