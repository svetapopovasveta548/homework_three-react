import ProductList from "../components/ProductList/ProductList"

export default function Home(props) {
  return  <ProductList removeFromFavorite={props.removeFromFavorite}  saveArrOfFavorite={props.saveArrOfFavorite}  saveArrOfProduct={props.saveArrOfProduct} item={props.productsArray} />

}
