import React, { useState, useEffect } from "react";
import Modal from "./components/Modal/Modal";
import { Routes, Route, NavLink } from "react-router-dom";
import Home from "./pages/Home";
import Cart from "./pages/Cart";
import Favorite from "./pages/Favorite";

function App() {
  const [product, setProducts] = useState([]);
  const [productCart, setToCart] = useState([]);
  const [productFavorite, setToFavorite] = useState([]);
  const [modal, openModal] = useState({
    isOpen: false,
    submitFunction: null,
    desc: null,
  });

  useEffect(() => {
    fetch("./product.json")
      .then((res) => res.json())
      .then((products) => setProducts(products));
  }, []);

  const saveArrOfProduct = (enteredProduct) => {
    // enteredProduct = props.data(карточка(объект))
    openModal((prevstate) => ({
      ...prevstate,
      isOpen: true,
      submitFunction: () => {
        setToCart((prev) => [...prev, enteredProduct]);
        openModal((prev) => ({ ...prev, isOpen: false, submitFunction: null }));
      },
      desc: "Do you want to add to cart?",
    }));
  };

  const saveArrOfFavorite = (enteredFavorite) => {
    setToFavorite((prev) => [...prev, enteredFavorite]);
  };

  const removeFromFavorite = (itemId2) => {
    const updatedGoal = productFavorite.filter((goal) => goal.id !== itemId2);
    setToFavorite(updatedGoal);
  };

  const onDeleteFromCart = (itemId) => {
    // itemId - id карточки
    // onDeleteItem срабатывает по клику на кнопку remove

    openModal((prev) => ({
      ...prev,
      isOpen: true,
      submitFunction: () => {
        setToCart((prev) => {
          const updatedGoals = prev.filter((goal) => goal.id !== itemId); //return false
          return updatedGoals;
        });
        openModal((prev) => ({ ...prev, isOpen: false, submitFunction: null }));
      },
      desc: "Do you want to remove from cart?",
    }));
  };

  const closeModal = () => {
    openModal((prevstate) => ({
      ...prevstate,
      isOpen: false,
    }));
  };

  return (
    <>
      <div>{productFavorite.length}</div>
      <nav>
        <NavLink to="/">Home</NavLink>
        <NavLink to="/favorite">Favorite</NavLink>
        <NavLink to="/cart">Cart</NavLink>
      </nav>
      <Routes>
        <Route
          path="/"
          element={
            <Home
              saveArrOfFavorite={saveArrOfFavorite}
              saveArrOfProduct={saveArrOfProduct}
              productsArray={product}
              removeFromFavorite={removeFromFavorite}
            />
          }
        />
        <Route
          path="/favorite"
          element={
            <Favorite
              removeFromFavorite={removeFromFavorite}
              productsFavorite={productFavorite}
            />
          }
        />
        <Route
          path="/cart"
          element={<Cart a={onDeleteFromCart} productsArray={productCart} />}
        />
      </Routes>
      {modal.isOpen && (
        <Modal
          desc={modal.desc}
          onClick2={modal.submitFunction}
          onClick={closeModal}
        />
      )}
    </>
  );
}

export default App;
