import React, { useState, useEffect } from "react";
import "./ProductCards.css";
import star from "./star-svgrepo-com (1).svg";
import star2 from "./star-gold-orange-svgrepo-com.svg";

function ProductCards(props) {
  const [isTrue, setIsTrue] = useState(false);
  const { title, price, url, id } = props.data;

  useEffect(() => {
    const isProductInArray = props.productsArray.filter(
      (item) => item.id === id
    );
    isProductInArray.length !== 0 &&
      props.page == "favorite" &&
      setIsTrue(true);
  }, [props.page]);

  const saveId = () => {
    props.c(id);
  };

  const toggle = () => {
    isTrue ? props.removeFromFavorite(id) : props.saveArrOfFavorite(props.data);

    setIsTrue(!isTrue);
  };

  const onClickSaveArrOfProduct = () => {
    //вызываю эту функцию
    props.saveArrOfProduct(props.data);
  };

  return (
    <div>
      <div className="wrapper-product">
        <div onClick={toggle}>
          <img className="star" src={isTrue ? star2 : star} alt="" />
        </div>

        <img className="img" src={url} alt="" />
        {title}
        <div>{price}</div>
        {props.c ? (
          <button className="btn-remove" onClick={saveId}>
            Remove from Basket
          </button>
        ) : (
          <button className="btn-add" onClick={onClickSaveArrOfProduct}>
            Add To Basket
          </button>
        )}
      </div>
    </div>
  );
}

export default ProductCards;
