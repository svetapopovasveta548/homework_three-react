import React from "react";
import "./Modal.css";
import modalWindowContent from "../Config/config";
import cross from "./cross-small-svgrepo-com (1).svg";

function Modal(props) {
  return (
    <div className="modal-background" onClick={props.onClick}>
      <div className="modal-wrapper" onClick={(e) => e.stopPropagation()}>
        <img className="cross" onClick={props.onClick} src={cross} alt="" />
        {/* {modalWindowContent.map((item) => (
          <p key={item.id}>{item.title}</p>
        ))} */}
        <p>{props.desc}</p>
        <button onClick={props.onClick2}>Yes</button>
        <button onClick={props.onClick}>No</button>
      </div>
    </div>
  );
}

export default Modal;
