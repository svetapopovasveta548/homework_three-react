import React, { useState } from "react";
import ProductCards from "../ProductCards/ProductCards";
import "./ProductList.css"

function ProductList(props) {
  return (
    <div className="wrapper">
      {props.item.map((items) => (
        <ProductCards
          c={props.b}
          productsArray={props.item}
          removeFromFavorite={props.removeFromFavorite}
          saveArrOfFavorite={props.saveArrOfFavorite}
          saveArrOfProduct={props.saveArrOfProduct}
          key={items.id}
          data={items}
        />
      ))}
    </div>
  );
}

export default ProductList;
